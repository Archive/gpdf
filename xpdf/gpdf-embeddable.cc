/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/**
 * application/x-pdf embeddable
 *
 * Authors:
 *   Michael Meeks <michael@ximian.com>
 *   Martin Kretzschmar <Martin.Kretzschmar@inf.tu-dresden.de>
 *
 * Copyright 1999, 2000 Ximian, Inc.
 * Copyright 2002 Martin Kretzschmar
 */

#include "gpdf-persist-stream.h"
#include "gpdf-embeddable.h"
#include "gpdf-embeddable-view.h"
#include "gpdf-util.h"

BEGIN_EXTERN_C

#define PARENT_TYPE BONOBO_EMBEDDABLE_TYPE
static BonoboEmbeddableClass *parent_class = NULL;

enum {
	LAST_SIGNAL
};

static guint gpdf_embeddable_signals [LAST_SIGNAL];

static void
destroy (GtkObject *object)
{
	GPdfEmbeddable *embeddable = GPDF_EMBEDDABLE (object);

	g_return_if_fail (object != NULL);
	g_return_if_fail (GPDF_IS_EMBEDDABLE (object));

	if (embeddable->priv->persist_stream) {
		bonobo_object_unref (
			BONOBO_OBJECT (embeddable->priv->persist_stream));
		embeddable->priv->persist_stream = NULL;
	}

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
finalize (GtkObject *object)
{
	g_free ((GPDF_EMBEDDABLE (object))->priv);

	if (GTK_OBJECT_CLASS (parent_class)->finalize)
		GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
class_init (GPdfEmbeddableClass *klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);

	parent_class = BONOBO_EMBEDDABLE_CLASS (gtk_type_class (PARENT_TYPE));

	object_class->destroy = destroy;
	object_class->finalize = finalize;
}

static void
init (GPdfEmbeddable *embeddable)
{
	embeddable->priv = g_new0 (GPdfEmbeddablePrivate, 1);
}

static BonoboView *
gpdf_embeddable_view_factory (BonoboEmbeddable *embeddable,
			      const Bonobo_ViewFrame view_frame,
			      void *closure)
{
	GPdfEmbeddableView *view = 
		gpdf_embeddable_view_new (GPDF_EMBEDDABLE (embeddable));

#ifdef PDF_DEBUG
	printf ("Returning new view\n");
#endif

	return BONOBO_VIEW (view);
}


GPdfEmbeddable *
gpdf_embeddable_construct (GPdfEmbeddable *embeddable, 
			   GPdfPersistStream *persist_stream)
{
	g_return_val_if_fail (embeddable != NULL, NULL);
	g_return_val_if_fail (GPDF_IS_EMBEDDABLE (embeddable), NULL);
	g_return_val_if_fail (persist_stream != NULL, NULL);
	g_return_val_if_fail (GPDF_IS_PERSIST_STREAM (persist_stream), NULL);

	bonobo_embeddable_construct (BONOBO_EMBEDDABLE (embeddable),
				     gpdf_embeddable_view_factory,
				     NULL);

	embeddable->priv->persist_stream = persist_stream;
	bonobo_object_ref (BONOBO_OBJECT (persist_stream));

	bonobo_object_add_interface (BONOBO_OBJECT (embeddable),
				     BONOBO_OBJECT (persist_stream));

	return embeddable;
}

GPdfEmbeddable *
gpdf_embeddable_new (GPdfPersistStream *persist_stream)
{
	GPdfEmbeddable *embeddable;

	g_return_val_if_fail (persist_stream != NULL, NULL);
	g_return_val_if_fail (GPDF_IS_PERSIST_STREAM (persist_stream), NULL);
	
	embeddable = GPDF_EMBEDDABLE (gtk_type_new (GPDF_TYPE_EMBEDDABLE));

#ifdef PDF_DEBUG
	g_message ("Created new GPdfEmbeddable %p", embeddable);
#endif

	return gpdf_embeddable_construct (embeddable, persist_stream);
}

E_MAKE_TYPE (gpdf_embeddable, "GPdfEmbeddable", GPdfEmbeddable,
	     class_init, init, PARENT_TYPE)

END_EXTERN_C
