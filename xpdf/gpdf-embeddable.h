/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/**
 * gpdf embeddable
 *
 * Author:
 *   Martin Kretzschmar <Martin.Kretzschmar@inf.tu-dresden.de>
 *   Michael Meeks <michael@ximian.com>
 *
 * Copyright 1999, 2000 Ximian, Inc.
 * Copyright 2002 Martin Kretzschmar
 */

#ifndef GPDF_EMBEDDABLE_H
#define GPDF_EMBEDDABLE_H

#define GString G_String
#define GList G_List
#include <bonobo.h>
#undef GList
#undef GString

#include "gpdf-persist-stream.h"
#include "PDFDoc.h"
#include "BonoboStream.h"
#include "gpdf-util.h"

BEGIN_C_DECLS

#define GPDF_TYPE_EMBEDDABLE            (gpdf_embeddable_get_type ())
#define GPDF_EMBEDDABLE(obj)            (GTK_CHECK_CAST ((obj), GPDF_TYPE_EMBEDDABLE, GPdfEmbeddable))
#define GPDF_EMBEDDABLE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GPDF_TYPE_EMBEDDABLE, GPdfEmbeddableClass))
#define GPDF_IS_EMBEDDABLE(obj)         (GTK_CHECK_TYPE ((obj), GPDF_TYPE_EMBEDDABLE))
#define GPDF_IS_EMBEDDABLE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPDF_TYPE_EMBEDDABLE))


typedef struct _GPdfEmbeddable        GPdfEmbeddable;
typedef struct _GPdfEmbeddableClass   GPdfEmbeddableClass;
typedef struct _GPdfEmbeddablePrivate GPdfEmbeddablePrivate;

struct _GPdfEmbeddable {
	BonoboEmbeddable       embeddable;

	GPdfEmbeddablePrivate *priv;
};

struct _GPdfEmbeddableClass {
	BonoboEmbeddableClass parent_class;
};

/* FIXME: make it really private (pdf-view still needs it) */
struct _GPdfEmbeddablePrivate {
	GPdfPersistStream *persist_stream;
};

GtkType         gpdf_embeddable_get_type  (void);
GPdfEmbeddable *gpdf_embeddable_new       (GPdfPersistStream *gpdf_persist_stream);
GPdfEmbeddable *gpdf_embeddable_construct (GPdfEmbeddable *embeddable,
					   GPdfPersistStream *gpdf_persist_stream);

END_C_DECLS

#endif /* GPDF_EMBEDDABLE_H */
