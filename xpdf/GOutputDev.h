//========================================================================
//
// GOutputDev.h
//
// Copyright 1999 Miguel de Icaza
//
//========================================================================

#ifndef GOUTPUTDEV_H
#define GOUTPUTDEV_H

#ifdef __GNUC__
#pragma interface
#endif

#include <stddef.h>
#include "config.h"
#include "Object.h"
#include "CharTypes.h"
#include "GlobalParams.h"
#include "OutputDev.h"
#define GList G_List
#define GString G_String
#  include <gdk/gdk.h>
#undef GString
#undef GList

class GString;
class GList;
struct GfxRGB;
class GfxFont;
class GfxSubpath;
class TextPage;
class GOutputFontCache;
struct T3FontCacheTag;
class T3FontCache;
struct T3GlyphStack;
class GOutputDev;
class Link;
class Catalog;
class DisplayFontParam;
class UnicodeMap;
class CharCodeToUnicode;

#if HAVE_T1LIB_H
class T1FontEngine;
class T1FontFile;
class T1Font;
#endif

#if FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)
class FTFontEngine;
class FTFontFile;
class FTFont;
#endif

#if !FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)
class TTFontEngine;
class TTFontFile;
class TTFont;
#endif

//------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------

#define maxRGBCube 7		// max size of RGB color cube

#define numTmpPoints 256	// number of XPoints in temporary array
#define numTmpSubpaths 16	// number of elements in temporary arrays
				//   for fill/clip

//------------------------------------------------------------------------
// Misc types
//------------------------------------------------------------------------

struct BoundingRect {
  short xMin, xMax;		// min/max x values
  short yMin, yMax;		// min/max y values
};

//------------------------------------------------------------------------
// GOutputFont
//------------------------------------------------------------------------

class GOutputFont {
public:

  // Constructor.
  GOutputFont(Ref *idA, double m11OrigA, double m12OrigA,
	      double m21OrigA, double m22OrigA,
	      double m11A, double m12A, double m21A, double m22A,
	      GOutputDev *gOutA);

  // Destructor.
  virtual ~GOutputFont();

  // Does this font match the ID and transform?
  GBool matches(Ref *idA, double m11OrigA, double m12OrigA,
		double m21OrigA, double m22OrigA)
    { return id.num == idA->num && id.gen == idA->gen &&
	     m11Orig == m11OrigA && m12Orig == m12OrigA &&
	     m21Orig == m21OrigA && m22Orig == m22OrigA; }

  // Was font created successfully?
  virtual GBool isOk() = 0;

  // Update <gc> with this font.
  virtual void updateGC(GdkGC *gc) = 0;
  
  // Draw character <c>/<u> at <x>,<y> (in device space).
  virtual void drawChar(GfxState *state, GdkPixmap *pixmap, int w, int h,
			GdkGC *gc, GfxRGB *rgb,
			double x, double y, double dx, double dy,
			CharCode c, Unicode *u, int uLen) = 0;

  // Returns true if this XOutputFont subclass provides the
  // getCharPath function.
  virtual GBool hasGetCharPath() { return gFalse; }

  // Add the character outline for <c>/<u> to the current path.
  virtual void getCharPath(GfxState *state,
			   CharCode c, Unicode *u, int ulen);

protected:

  Ref id;			// font ID
  double m11Orig, m12Orig,	// original transform matrix
         m21Orig, m22Orig;
  double m11, m12, m21, m22;	// actual transform matrix (possibly
				//   modified for font substitution)
  GOutputDev *gOut;
};

#if HAVE_T1LIB_H
//------------------------------------------------------------------------
// GOutputT1Font
//------------------------------------------------------------------------

class GOutputT1Font: public GOutputFont {
public:

  GOutputT1Font(Ref *idA, T1FontFile *fontFileA,
		double m11OrigA, double m12OrigA,
		double m21OrigA, double m22OrigA,
		double m11A, double m12A,
		double m21A, double m22A,
		GOutputDev *gOutA);

  virtual ~GOutputT1Font();

  // Was font created successfully?
  virtual GBool isOk();

  // Update <gc> with this font.
  virtual void updateGC(GdkGC *gc);

  // Draw character <c>/<u> at <x>,<y>.
  virtual void drawChar(GfxState *state, GdkPixmap *pixmap, int w, int h,
			GdkGC *gc, GfxRGB *rgb,
			double x, double y, double dx, double dy,
			CharCode c, Unicode *u, int uLen);

  // Returns true if this XOutputFont subclass provides the
  // getCharPath function.
  virtual GBool hasGetCharPath() { return gTrue; }

  // Add the character outline for <c>/<u> to the current path.
  virtual void getCharPath(GfxState *state,
			   CharCode c, Unicode *u, int ulen);

private:

  T1FontFile *fontFile;
  T1Font *font;
};
#endif // HAVE_T1LIB_H

#if FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)
//------------------------------------------------------------------------
// GOutputFTFont
//------------------------------------------------------------------------

class GOutputFTFont: public GOutputFont {
public:

  GOutputFTFont(Ref *idA, FTFontFile *fontFileA,
		double m11OrigA, double m12OrigA,
		double m21OrigA, double m22OrigA,
		double m11A, double m12A,
		double m21A, double m22A,
		GOutputDev *gOutA);

  virtual ~GOutputFTFont();

  // Was font created successfully?
  virtual GBool isOk();

  // Update <gc> with this font.
  virtual void updateGC(GdkGC *gc);

  // Draw character <c>/<u> at <x>,<y>.
  virtual void drawChar(GfxState *state, GdkPixmap *pixmap, int w, int h,
			GdkGC *gc, GfxRGB *rgb,
			double x, double y, double dx, double dy,
			CharCode c, Unicode *u, int uLen);

  // Returns true if this XOutputFont subclass provides the
  // getCharPath function.
  virtual GBool hasGetCharPath() { return gTrue; }

  // Add the character outline for <c>/<u> to the current path.
  virtual void getCharPath(GfxState *state,
			   CharCode c, Unicode *u, int ulen);

private:

  FTFontFile *fontFile;
  FTFont *font;
};
#endif // FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)

#if !FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)
//------------------------------------------------------------------------
// GOutputTTFont
//------------------------------------------------------------------------

class GOutputTTFont: public GOutputFont {
public:

  GOutputTTFont(Ref *idA, TTFontFile *fontFileA,
		double m11OrigA, double m12OrigA,
		double m21OrigA, double m22OrigA,
		double m11A, double m12A,
		double m21A, double m22A,
		GOutputDev *gOutA);

  virtual ~GOutputTTFont();

  // Was font created successfully?
  virtual GBool isOk();

  // Update <gc> with this font.
  virtual void updateGC(GdkGC *gc);

  // Draw character <c>/<u> at <x>,<y>.
  virtual void drawChar(GfxState *state, GdkPixmap *pixmap, int w, int h,
			GdkGC *gc, GfxRGB *rgb,
			double x, double y, double dx, double dy,
			CharCode c, Unicode *u, int uLen);

private:

  TTFontFile *fontFile;
  TTFont *font;
};
#endif // !FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)

//------------------------------------------------------------------------
// GOutputServer8BitFont
//------------------------------------------------------------------------

class GOutputServer8BitFont: public GOutputFont {
public:

  GOutputServer8BitFont(Ref *idA, GString *xlfdFmt,
			UnicodeMap *xUMapA, CharCodeToUnicode *fontUMap,
			double m11OrigA, double m12OrigA,
			double m21OrigA, double m22OrigA,
			double m11A, double m12A, double m21A, double m22A,
			GOutputDev *gOutA);

  virtual ~GOutputServer8BitFont();

  // Was font created successfully?
  virtual GBool isOk();

  // Update <gc> with this font.
  virtual void updateGC(GdkGC *gc);

  // Draw character <c>/<u> at <x>,<y>.
  virtual void drawChar(GfxState *state, GdkPixmap *pixmap, int w, int h,
			GdkGC *gc, GfxRGB *rgb,
			double x, double y, double dx, double dy,
			CharCode c, Unicode *u, int uLen);

private:

  GdkFont *gdkFont;		// the GDK font
  Gushort map[256];		// forward map (char code -> X font code)
  UnicodeMap *xUMap;
};

//------------------------------------------------------------------------
// GOutputServer16BitFont
//------------------------------------------------------------------------

class GOutputServer16BitFont: public GOutputFont {
public:

  GOutputServer16BitFont(Ref *idA, GString *xlfdFmt,
			 UnicodeMap *xUMapA, CharCodeToUnicode *fontUMap,
			 double m11OrigA, double m12OrigA,
			 double m21OrigA, double m22OrigA,
			 double m11A, double m12A, double m21A, double m22A,
			 GOutputDev *gOutA);

  virtual ~GOutputServer16BitFont();

  // Was font created successfully?
  virtual GBool isOk();

  // Update <gc> with this font.
  virtual void updateGC(GdkGC *gc);

  // Draw character <c>/<u> at <x>,<y>.
  virtual void drawChar(GfxState *state, GdkPixmap *pixmap, int w, int h,
			GdkGC *gc, GfxRGB *rgb,
			double x, double y, double dx, double dy,
			CharCode c, Unicode *u, int uLen);

private:

  GdkFont *gdkFont;		// the GDK font
  UnicodeMap *xUMap;
};

#if HAVE_T1LIB_H
class GOutputT1FontFile {
public:
  GOutputT1FontFile(int numA, int genA, GBool substA, T1FontFile *fontFileA)
    { num = numA; gen = genA; subst = substA; fontFile = fontFileA; }
  ~GOutputT1FontFile();
  int num, gen;
  GBool subst;
  T1FontFile *fontFile;
};
#endif

#if FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)
class GOutputFTFontFile {
public:
  GOutputFTFontFile(int numA, int genA, GBool substA, FTFontFile *fontFileA)
    { num = numA; gen = genA; subst = substA; fontFile = fontFileA; }
  ~GOutputFTFontFile();
  int num, gen;
  GBool subst;
  FTFontFile *fontFile;
};
#endif

#if !FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)
class GOutputTTFontFile {
public:
  GOutputTTFontFile(int numA, int genA, GBool substA, TTFontFile *fontFileA)
    { num = numA; gen = genA; subst = substA; fontFile = fontFileA; }
  ~GOutputTTFontFile();
  int num, gen;
  GBool subst;
  TTFontFile *fontFile;
};
#endif


//------------------------------------------------------------------------
// GOutputFontCache
//------------------------------------------------------------------------

class GOutputFontCache {
public:

  // Constructor.
  GOutputFontCache(GOutputDev *gOutA,
		   FontRastControl t1libControlA,
		   FontRastControl freetypeControlA);

  // Destructor.
  ~GOutputFontCache();

  // Initialize (or re-initialize) the font cache for a new document.
  void startDoc(int rMul, int gMul, int bMul,
		int rShift, int gShift, int bShift);

  // Get a font.  This creates a new font if necessary.
  GOutputFont *getFont(XRef *xref, GfxFont *gfxFont, double m11, double m12,
		       double m21, double m22);

private:

  void delFonts();
  void clear();
  GOutputFont *tryGetFont(XRef *xref, DisplayFontParam *dfp, GfxFont *gfxFont,
			  double m11Orig, double m12Orig,
			  double m21Orig, double m22Orig,
			  double m11, double m12, double m21, double m22,
			  GBool subst);
#if HAVE_T1LIB_H
  GOutputFont *tryGetT1Font(XRef *xref, GfxFont *gfxFont,
			    double m11, double m12, double m21, double m22);
  GOutputFont *tryGetT1FontFromFile(XRef *xref, GString *fileName,
				    GfxFont *gfxFont,
				    double m11Orig, double m12Orig,
				    double m21Orig, double m22Orig,
				    double m11, double m12,
				    double m21, double m22, GBool subst);
#endif
#if FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)
  GOutputFont *tryGetFTFont(XRef *xref, GfxFont *gfxFont,
			    double m11, double m12, double m21, double m22);
  GOutputFont *tryGetFTFontFromFile(XRef *xref, GString *fileName,
				    GfxFont *gfxFont,
				    double m11Orig, double m12Orig,
				    double m21Orig, double m22Orig,
				    double m11, double m12,
				    double m21, double m22, GBool subst);
#endif
#if !FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)
  GOutputFont *tryGetTTFont(XRef *xref, GfxFont *gfxFont,
			    double m11, double m12, double m21, double m22);
  GOutputFont *tryGetTTFontFromFile(XRef *xref, GString *fileName,
				    GfxFont *gfxFont,
				    double m11Orig, double m12Orig,
				    double m21Orig, double m22Orig,
				    double m11, double m12,
				    double m21, double m22, GBool subst);
#endif
  GOutputFont *tryGetServerFont(GString *xlfd, GString *encodingName,
				GfxFont *gfxFont,
				double m11Orig, double m12Orig,
				double m21Orig, double m22Orig,
				double m11, double m12,
				double m21, double m22);

  GOutputDev *gOut;

  GOutputFont *
    fonts[xOutFontCacheSize];
  int nFonts;

#if HAVE_T1LIB_H
  FontRastControl t1libControl;	// t1lib settings
  T1FontEngine *t1Engine;	// Type 1 font engine
  GList *t1FontFiles;		// list of Type 1 font files
				//   [GOutputT1FontFile]
#endif

#if HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H
  FontRastControl		// FreeType settings
    freetypeControl;
#endif
#if FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)
  FTFontEngine *ftEngine;	// FreeType font engine
  GList *ftFontFiles;		// list of FreeType font files
				//   [GOutputFTFontFile]
#endif
#if !FREETYPE2 && (HAVE_FREETYPE_FREETYPE_H || HAVE_FREETYPE_H)
  TTFontEngine *ttEngine;	// TrueType font engine
  GList *ttFontFiles;		// list of TrueType font files
				//   [XOutputTTFontFile]
#endif
};

//------------------------------------------------------------------------
// GOutputState
//------------------------------------------------------------------------

struct GOutputState {
  GdkGC *strokeGC;
  GdkGC *fillGC;
  GdkRegion *clipRegion;
  GOutputState *next;
};

//------------------------------------------------------------------------
// GOutputDev
//------------------------------------------------------------------------

class GOutputDev: public OutputDev {
public:

  // Constructor.
  GOutputDev(GdkPixmap *pixmapA, GBool reverseVideoA, GdkColor paperColor, 
	     GdkWindow *window);

  // Destructor.
  virtual ~GOutputDev();

  //---- get info about output device

  // Does this device use upside-down coordinates?
  // (Upside-down means (0,0) is the top left corner of the page.)
  virtual GBool upsideDown() { return gTrue; }

  // Does this device use drawChar() or drawString()?
  virtual GBool useDrawChar() { return gTrue; }

  // Does this device use beginType3Char/endType3Char?  Otherwise,
  // text in Type 3 fonts will be drawn with drawChar/drawString.
  virtual GBool interpretType3Chars() { return gTrue; }

  //----- initialization and control

  // Start a page.
  virtual void startPage(int pageNum, GfxState *state);

  // End a page.
  virtual void endPage();

  //----- link borders
  virtual void drawLink(Link *link, Catalog *catalog);

  //----- save/restore graphics state
  virtual void saveState(GfxState *state);
  virtual void restoreState(GfxState *state);

  //----- update graphics state
  virtual void updateAll(GfxState *state);
  virtual void updateCTM(GfxState *state, double m11, double m12,
			 double m21, double m22, double m31, double m32);
  virtual void updateLineDash(GfxState *state);
  virtual void updateFlatness(GfxState *state);
  virtual void updateLineJoin(GfxState *state);
  virtual void updateLineCap(GfxState *state);
  virtual void updateMiterLimit(GfxState *state);
  virtual void updateLineWidth(GfxState *state);
  virtual void updateFillColor(GfxState *state);
  virtual void updateStrokeColor(GfxState *state);

  //----- update text state
  virtual void updateFont(GfxState *state);

  //----- path painting
  virtual void stroke(GfxState *state);
  virtual void fill(GfxState *state);
  virtual void eoFill(GfxState *state);

  //----- path clipping
  virtual void clip(GfxState *state);
  virtual void eoClip(GfxState *state);

  //----- text drawing
  virtual void beginString(GfxState *state, GString *s);
  virtual void endString(GfxState *state);
  virtual void drawChar(GfxState *state, double x, double y,
			double dx, double dy,
			double originX, double originY,
			CharCode code, Unicode *u, int uLen);
  virtual GBool beginType3Char(GfxState *state,
			       CharCode code, Unicode *u, int uLen);
  virtual void endType3Char(GfxState *state);

  //----- image drawing
  virtual void drawImageMask(GfxState *state, Object *ref, Stream *str,
			     int width, int height, GBool invert,
			     GBool inlineImg);
  virtual void drawImage(GfxState *state, Object *ref, Stream *str,
			 int width, int height, GfxImageColorMap *colorMap,
			 int *maskColors, GBool inlineImg);

  //----- Type 3 font operators
  virtual void type3D0(GfxState *state, double wx, double wy);
  virtual void type3D1(GfxState *state, double wx, double wy,
		       double llx, double lly, double urx, double ury);

  //----- special access

  // Called to indicate that a new PDF document has been loaded.
  void startDoc(XRef *xrefA);

  // Find a string.  If <top> is true, starts looking at <xMin>,<yMin>;
  // otherwise starts looking at top of page.  If <bottom> is true,
  // stops looking at <xMax>,<yMax>; otherwise stops looking at bottom
  // of page.  If found, sets the text bounding rectange and returns
  // true; otherwise returns false.
  GBool findText(Unicode *s, int len, GBool top, GBool bottom,
		 int *xMin, int *yMin, int *xMax, int *yMax);

  // Get the text which is inside the specified rectangle.
  GString *getText(int xMin, int yMin, int xMax, int yMax);

  GBool isReverseVideo() { return reverseVideo; }

protected:

  // Update pixmap ID after a page change.
  void setPixmap(GdkPixmap *pixmap1, int pixmapW1, int pixmapH1)
    { pixmap = pixmap1; pixmapW = pixmapW1; pixmapH = pixmapH1; }

private:

  XRef *xref;			// the xref table for this PDF file
  int screenNum;		// X screen number
  GdkPixmap *pixmap;		// pixmap to draw into
  int pixmapW, pixmapH;		// size of pixmap
  gint depth;			// pixmap depth
  int flatness;			// line flatness
  GdkGC *paperGC;		// GC for background
  GdkGC *strokeGC;		// GC with stroke color
  GdkGC *fillGC;		// GC with fill color
  GdkRegion *clipRegion;	// clipping region
  GBool reverseVideo;		// reverse video mode
  GdkPoint			// temporary points array
    tmpPoints[numTmpPoints];
  int				// temporary arrays for fill/clip
    tmpLengths[numTmpSubpaths];
  BoundingRect
    tmpRects[numTmpSubpaths];
  GfxFont *gfxFont;		// current PDF font
  GOutputFont *font;		// current font
  GOutputFontCache *fontCache;	// font cache
  T3FontCache *			// Type 3 font cache
    t3FontCache[xOutT3FontCacheSize];
  int nT3Fonts;			// number of valid entries in t3FontCache
  T3GlyphStack *t3GlyphStack;	// Type 3 glyph context stack
  GOutputState *save;		// stack of saved states

  TextPage *text;		// text from the current page

  void updateLineAttrs(GfxState *state, GBool updateDash);
  void doFill(GfxState *state, GdkFillRule rule);
  void doClip(GfxState *state, GdkFillRule rule);
  int  convertPath(GfxState *state, GdkPoint **points, int *size,
		   int *numPoints, int **lengths, GBool fillHack);
  void convertSubpath(GfxState *state, GfxSubpath *subpath,
		      GdkPoint **points, int *size, int *n);
  void doCurve(GdkPoint **points, int *size, int *k,
	       double x0, double y0, double x1, double y1,
	       double x2, double y2, double x3, double y3);
  void addPoint(GdkPoint **points, int *size, int *k, int x, int y);
  void drawType3Glyph(T3FontCache *t3Font,
		      T3FontCacheTag *tag, Guchar *data,
		      double x, double y, GfxRGB *color);
  GdkColor *findColor(GfxRGB *rgb);
  Gulong findColor(GfxRGB *x, GfxRGB *err);
};

#endif
