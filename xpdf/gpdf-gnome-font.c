/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Additional font functions.
 *
 *  Copyright (C) 2000-2001 Ximian Inc.
 *            (C) 2002 Martin Kretzschmar
 *
 *  Author:
 *    Martin Kretzschmar <Martin.Kretzschmar@inf.tu-dresden.de>
 *
 * GPdf is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GPdf is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <libgnomeprint/gnome-print.h>
/* #include <libgnomeprint/private/gnome-font-private.h> doesn't work */

/* 
 * Cut & paste from private gnome-print headers
 */
typedef struct _GPFontMap GPFontMap;
typedef struct _GPFontEntry GPFontEntry;

typedef enum {
	GP_FONT_ENTRY_UNKNOWN,
	GP_FONT_ENTRY_TYPE1, /* Just ordinary Type1 font */
	GP_FONT_ENTRY_TRUETYPE,
	GP_FONT_ENTRY_TYPE1_ALIAS, /* Type1 font with foreign afm */
	GP_FONT_ENTRY_ALIAS, /* Full alias */
	GP_FONT_ENTRY_SPECIAL
} GPFontEntryType;

struct _GPFontEntry {
	GPFontEntryType type;
	gint refcount;
	/* Our face */
	GnomeFontFace * face;
	/* Common fields */
	gchar *name;
	gchar *version;
	gchar *familyname;
	gchar *speciesname;
	gchar *psname;
	/* fixme: fixme: fixme: */
	gchar *weight;
	/* Some parsed afm latin metrics */
	GnomeFontWeight Weight;
	gdouble ItalicAngle; /* italic < 0 */
};

struct _GnomeFontFace {
	GObject object;

	/* Pointer to our fontmap entry */
	GPFontEntry * entry;
	/* Glyph storage */
	gint num_glyphs;
	/* GFFGlyphInfo */ void *glyphs;
	/* FT -> PostScript scaling coefficent */
	gdouble ft2ps;
	/* Face bounding box */
	ArtDRect bbox;
	/* FreeType stuff */
	/* FT_Face */ void *ft_face;
	/* Our fonts */
	GSList *fonts;
};

GPFontMap * gp_fontmap_get (void);
void gp_fontmap_release (GPFontMap *);

void gp_font_entry_ref (GPFontEntry *entry);
void gp_font_entry_unref (GPFontEntry *entry);

GPFontEntry *gp_font_entry_from_files (GPFontMap *map,
				       const guchar *name,
				       const guchar *family,
				       const guchar *species, gboolean hidden,
				       const guchar *filename, gint face,
				       const GSList *additional);

/*
 * Creates new face and creates link with FontEntry
 * From libgnomeprint/gnome-font-face.c
 */
static void
gff_face_from_entry (GPFontEntry *e)
{
	GnomeFontFace *face;

	g_return_if_fail (e->face == NULL);

	face = g_object_new (GNOME_TYPE_FONT_FACE, NULL);

	gp_font_entry_ref (e);
	face->entry = e;
	e->face = face;
}

/*
 * End cut & paste
 */

/* This won't work for BonoboPrint */
GnomeFontFace *
gpdf_gnome_print_add_font_file (GnomePrintContext *pc,
			   const guchar *name, const guchar *family,
			   const guchar *species, const guchar *filename,
			   gint subface)
{
	GPFontMap *map;
	GPFontEntry *fe;

	g_return_val_if_fail (pc != NULL, NULL);

	map = gp_fontmap_get ();

	fe = gp_font_entry_from_files (map, name, family, species,
				       TRUE, filename, subface, NULL);
	gp_fontmap_release (map);

	gff_face_from_entry (fe);
	g_assert (fe->face != NULL);

	return fe->face;
}

static void
unlink_font_file_cb (gpointer data, GObject *object)
{
	unlink ((gchar *)data);
}

GnomeFontFace *
gpdf_gnome_print_add_font (GnomePrintContext *pc,
			   const guchar *name, const guchar *family,
			   const guchar *species,
			   const guchar *fontdata, gsize length,
		      gint subface)
{
	GPFontMap *map;
	GPFontEntry *fe;
	gchar *tmpname;
	gint fd;
	FILE *f;
	size_t written;
	GnomeFontFace *result;

	g_return_val_if_fail (pc != NULL, NULL);
	g_return_val_if_fail (fontdata != NULL, NULL);
	g_return_val_if_fail (length > 0, NULL);

	fd = g_file_open_tmp ("gpdf-XXXXXX", &tmpname, NULL);
	g_assert (fd > -1);
	
	f = fdopen (fd, "wb");
	written = fwrite (fontdata, length, 1, f);
	fclose (f);
	g_assert (written = length);
	
	result = gpdf_gnome_print_add_font_file (pc, name, family, species,
						 tmpname, subface);
	
	g_object_weak_ref (G_OBJECT (result),
			   unlink_font_file_cb,
			   tmpname);

	return result;
}
