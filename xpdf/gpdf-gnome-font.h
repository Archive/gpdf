/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Additional font functions to be included in Gnome Print (hopefully)
 *
 *            (C) 2002 Martin Kretzschmar
 *
 *  Author:
 *    Martin Kretzschmar <Martin.Kretzschmar@inf.tu-dresden.de>
 *
 * GPdf is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GPdf is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <libgnomeprint/gnome-print.h>

G_BEGIN_DECLS

GnomeFontFace *
gpdf_gnome_print_add_font (GnomePrintContext *pc,
		      const guchar *name, const guchar *family,
		      const guchar *species,
		      const guchar *fontdata, gsize length,
		      gint subface);

G_END_DECLS
