/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/**
 * application/x-pdf embeddable view
 *
 * Author:
 *   Martin Kretzschmar <Martin.Kretzschmar@inf.tu-dresden.de>
 *   Michael Meeks <michael@ximian.com>
 *
 * Copyright 1999, 2000 Ximian, Inc.
 * Copyright 2002 Martin Kretzschmar
 */

#ifndef GPDF_EMBEDDABLE_VIEW_H
#define GPDF_EMBEDDABLE_VIEW_H

extern "C" {
#define GString G_String
#define GList G_List
#include <gtk/gtk.h>
#include <bonobo.h>
#include "gtkgesture.h"
#undef GList
#undef GString
}
#include "GOutputDev.h"
#include "gpdf-embeddable.h"
#include "gpdf-util.h"

BEGIN_C_DECLS

#define GPDF_TYPE_EMBEDDABLE_VIEW            (gpdf_embeddable_view_get_type ())
#define GPDF_EMBEDDABLE_VIEW(obj)            (GTK_CHECK_CAST ((obj), GPDF_TYPE_EMBEDDABLE_VIEW, GPdfEmbeddableView))
#define GPDF_EMBEDDABLE_VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GPDF_TYPE_EMBEDDABLE_VIEW, GPdfEmbeddableViewClass))
#define GPDF_IS_EMBEDDABLE_VIEW(obj)         (GTK_CHECK_TYPE ((obj), GPDF_TYPE_EMBEDDABLE_VIEW))
#define GPDF_IS_EMBEDDABLE_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GPDF_TYPE_EMBEDDABLE_VIEW))


typedef struct _GPdfEmbeddableView      GPdfEmbeddableView;
typedef struct _GPdfEmbeddableViewClass GPdfEmbeddableViewClass;



struct _GPdfEmbeddableView {
	BonoboView            view;

	GtkWidget            *scrolled_window;
	GtkWidget            *mainbox;
};

struct _GPdfEmbeddableViewClass {
	BonoboViewClass parent_class;
};

GtkType             gpdf_embeddable_view_get_type  (void);
GPdfEmbeddableView *gpdf_embeddable_view_new       (GPdfEmbeddable *embeddable);
GPdfEmbeddableView *gpdf_embeddable_view_construct (GPdfEmbeddableView *embeddable_view, GPdfEmbeddable *embeddable);

END_C_DECLS

#endif /* GPDF_EMBEDDABLE_VIEW_H */
