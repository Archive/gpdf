/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/**
 * gpdf --  GTK widget side of GOutputDev
 *
 * Author:
 *   Michael Meeks <michael@ximian.com>
 *   Martin Kretzschmar <Martin.Kretzschmar@inf.tu-dresden.de>
 *
 * Copyright 1999, 2000 Ximian, Inc.
 * Copyright 2002 Martin Kretzschmar
 */

#include "PDFDoc.h"
#include "pdf-view.h"
#include "gpdf-persist-stream.h"
#include "gpdf-util.h"
#include <math.h>

#define PDF_DEBUG

BEGIN_EXTERN_C

struct _PdfViewPrivate {
	GtkGestureHandler *gh;

	PDFDoc *pdf_doc;

	GtkPixmap         *pixmap;
	GOutputDev        *out;
	GdkColor           paper;
	gint               w, h;
	gdouble            zoom;
	gint               page;
};

#define IS_NON_NULL_PDF_VIEW(obj) \
(((obj) != NULL) && (IS_PDF_VIEW ((obj))))

#define IS_PDF_VIEW_WITH_DOC(obj) \
(IS_NON_NULL_PDF_VIEW(obj) && (obj)->priv->pdf_doc)

enum {
	ZOOM_IN_SIGNAL,
	ZOOM_OUT_SIGNAL,
	ZOOM_TO_FIT_SIGNAL,
	ZOOM_TO_FIT_WIDTH_SIGNAL,
	ZOOM_TO_DEFAULT_SIGNAL,
	LAST_SIGNAL
};

static guint pdf_view_signals [LAST_SIGNAL];


/*
 * different size ?
 */
static gboolean
setup_size (PdfView *pdf_view)
{
	int      w, h;
	gboolean same;
	PdfViewPrivate *priv;
	PDFDoc *pdf_doc;

	g_return_val_if_fail (IS_NON_NULL_PDF_VIEW (pdf_view), FALSE);

	priv = pdf_view->priv;

	if (priv->pdf_doc == NULL) {
		priv->w = 320;
		priv->h = 200;
		return FALSE;
	}

	pdf_doc = priv->pdf_doc;

	w = (int)((pdf_doc->getPageWidth (priv->page) * priv->zoom) / 72.0);
	h = (int)((pdf_doc->getPageHeight (priv->page) * priv->zoom) / 72.0);

	same = (priv->w == w) && (priv->h == h);

	priv->w = w;
	priv->h = h;

	return same;
}

static void
setup_pixmap (PdfView *pdf_view)
{
	GdkPixmap *pixmap = NULL;
	GdkWindow *window;
	PdfViewPrivate *priv;

	g_return_if_fail (IS_PDF_VIEW_WITH_DOC (pdf_view));

	priv = pdf_view->priv;

	if (setup_size (pdf_view) && priv->pixmap) {
#ifdef PDF_DEBUG
		g_message ("No need to re-init output device");
#endif
		return;
	}

	window = gtk_widget_get_parent_window (GTK_WIDGET (pdf_view));

	pixmap = gdk_pixmap_new (window, priv->w, priv->h, -1);

	gdk_color_white (gtk_widget_get_default_colormap(), &priv->paper);
	priv->out = new GOutputDev (pixmap, gFalse, priv->paper, window);

	if (priv->pixmap)
		gtk_widget_destroy (GTK_WIDGET (priv->pixmap));
	priv->pixmap = GTK_PIXMAP (gtk_pixmap_new (pixmap, NULL));
	gtk_widget_set_double_buffered (GTK_WIDGET (priv->pixmap), FALSE);
	gtk_container_add (GTK_CONTAINER (pdf_view),
			   GTK_WIDGET (priv->pixmap));
	gtk_widget_show_all (GTK_WIDGET (pdf_view));
}

static  void
render_page (PdfView *pdf_view)
{
	PDFDoc *pdf_doc;
	PdfViewPrivate *priv;

	g_return_if_fail (IS_PDF_VIEW_WITH_DOC (pdf_view));

	priv = pdf_view->priv;

#ifdef PDF_DEBUG
	g_message ("Redraw view of page %d", priv->page);
#endif

	setup_pixmap (pdf_view);

	pdf_doc = priv->pdf_doc;
	priv->out->startDoc (pdf_doc->getXRef ());
	pdf_doc->displayPage (priv->out,
			      priv->page,
			      (int) priv->zoom,
			      0, /* gTrue */ gFalse);

	gtk_widget_queue_draw (GTK_WIDGET (pdf_view));
}


#define PARENT_TYPE GTK_TYPE_EVENT_BOX
static GtkEventBoxClass *parent_class = NULL;

#define MIN_ZOOM_FACTOR  28.9
#define MAX_ZOOM_FACTOR 179.2

void
pdf_view_page_prev (PdfView *pdf_view)
{
	g_return_if_fail (IS_PDF_VIEW_WITH_DOC (pdf_view));

	if (pdf_view->priv->page > 1) {
		pdf_view->priv->page--;
		render_page (pdf_view);
	}
}

void
pdf_view_page_next (PdfView *pdf_view)
{
	int last_page;

	g_return_if_fail (IS_PDF_VIEW_WITH_DOC (pdf_view));

	last_page = pdf_view->priv->pdf_doc->getNumPages ();

	if (pdf_view->priv->page < last_page) {
		pdf_view->priv->page++;
		render_page (pdf_view);
	}
}

void
pdf_view_page_first (PdfView *pdf_view)
{
	g_return_if_fail (IS_PDF_VIEW_WITH_DOC (pdf_view));

	if (pdf_view->priv->page != 1) {
		pdf_view->priv->page = 1;
		render_page (pdf_view);
	}
}

void
pdf_view_page_last (PdfView *pdf_view)
{
	int last_page;

	g_return_if_fail (IS_PDF_VIEW_WITH_DOC (pdf_view));

	last_page = pdf_view->priv->pdf_doc->getNumPages ();

	if (pdf_view->priv->page != last_page) {
		pdf_view->priv->page = last_page;
		render_page (pdf_view);
	}
}


double
pdf_view_get_zoom (PdfView *pdf_view)
{
	g_return_val_if_fail (IS_NON_NULL_PDF_VIEW (pdf_view), 0.0);

	return pdf_view->priv->zoom;
}

#define DOUBLE_EQUAL(a,b) (fabs (a - b) < 1e-6)
void
pdf_view_set_zoom (PdfView *pdf_view, gdouble new_zoom)
{
	g_return_if_fail (IS_NON_NULL_PDF_VIEW (pdf_view));
	g_return_if_fail (new_zoom > 0.0);

	new_zoom = CLAMP (new_zoom, MIN_ZOOM_FACTOR, MAX_ZOOM_FACTOR);
	if (DOUBLE_EQUAL (pdf_view->priv->zoom, new_zoom))
		return;
			
	pdf_view->priv->zoom = new_zoom;

	if (pdf_view->priv->pdf_doc != NULL)
		render_page (pdf_view);
}

double
pdf_view_get_page_width (PdfView *pdf_view)
{
	PDFDoc *pdf_doc;

	g_return_val_if_fail (IS_PDF_VIEW_WITH_DOC (pdf_view), 0.0);

	pdf_doc = pdf_view->priv->pdf_doc;
	return pdf_doc->getPageWidth (pdf_view->priv->page);
}

double
pdf_view_get_page_height (PdfView *pdf_view)
{
	PDFDoc *pdf_doc;

	g_return_val_if_fail (IS_PDF_VIEW_WITH_DOC (pdf_view), 0.0);

	pdf_doc = pdf_view->priv->pdf_doc;
	return pdf_doc->getPageHeight (pdf_view->priv->page);
}


/*
 * GtkWidget key_press method override
 */
static gboolean
pv_key_press_event (GtkWidget *widget, GdkEventKey *event)
{
	g_return_val_if_fail (IS_NON_NULL_PDF_VIEW (widget), FALSE);

	switch (event->keyval) {
		/*
		 * Various shortcuts mapped to verbs.
		 */
	case GDK_Home:
		pdf_view_page_first (PDF_VIEW (widget));
		break;
	case GDK_End:
		pdf_view_page_last (PDF_VIEW (widget));
		break;
	case GDK_Page_Down:
		pdf_view_page_next (PDF_VIEW (widget));
		break;
	case GDK_Page_Up:
		pdf_view_page_prev (PDF_VIEW (widget));
		break;
	case GDK_plus:
	case GDK_KP_Add:
	case GDK_equal: /* FIXME this is keyboard layout dependent? */
		gtk_signal_emit (GTK_OBJECT (widget),
				 pdf_view_signals [ZOOM_IN_SIGNAL]);
		break;
	case GDK_minus:
	case GDK_KP_Subtract:
	case GDK_underscore: /* ditto */
		gtk_signal_emit (GTK_OBJECT (widget),
				 pdf_view_signals [ZOOM_OUT_SIGNAL]);
		break;
	case GDK_1:
		gtk_signal_emit (GTK_OBJECT (widget),
				 pdf_view_signals [ZOOM_TO_DEFAULT_SIGNAL]);
		break;
	case GDK_f:
	case GDK_F:
		gtk_signal_emit (GTK_OBJECT (widget), 
				 pdf_view_signals [ZOOM_TO_FIT_SIGNAL]);
		break;
	default:
		return FALSE;
	}

	/* Avoid doing keyboard navigation if we handle this event. */
	gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "key_press_event");
	return TRUE;
}

static gboolean
pv_button_press_event (GtkWidget *widget, GdkEventButton *event)
{
	if (event->button == 1 && !GTK_WIDGET_HAS_FOCUS (widget)) {
		gtk_widget_grab_focus (widget);
		return TRUE;
	} else
		return FALSE;
}

static void
gesture_page_next_event_cb (GtkGestureHandler *gh, PdfView *pdf_view, gpointer p)
{
	pdf_view_page_next (pdf_view);
}

static void
gesture_page_prev_event_cb (GtkGestureHandler *gh, PdfView *pdf_view, gpointer p)
{
	pdf_view_page_prev (pdf_view);
}

static void
gesture_page_first_event_cb (GtkGestureHandler *gh, PdfView *pdf_view,
			     gpointer p)
{
	pdf_view_page_first (pdf_view);
}

static void
gesture_page_last_event_cb (GtkGestureHandler *gh, PdfView *pdf_view, gpointer p)
{
	pdf_view_page_last (pdf_view);
}

static void
gesture_zoom_in_event_cb (GtkGestureHandler *gh, PdfView *pdf_view, gpointer p)
{
		gtk_signal_emit (GTK_OBJECT (pdf_view),
				 pdf_view_signals [ZOOM_IN_SIGNAL]);
}

static void
gesture_zoom_out_event_cb (GtkGestureHandler *gh, PdfView *pdf_view, gpointer p)
{
		gtk_signal_emit (GTK_OBJECT (pdf_view),
				 pdf_view_signals [ZOOM_OUT_SIGNAL]);
}


void
pdf_view_set_pdf_doc (PdfView *pdf_view, PDFDoc *pdf_doc)
{
	PdfViewPrivate *priv;

	g_return_if_fail (IS_NON_NULL_PDF_VIEW (pdf_view));

	priv = pdf_view->priv;
	
	/* 
	 * All PDFDocs in GPdf code are owned by a GPdfPersistStream,
	 * so don't delete our old pdf_doc
	 */
	
	priv->pdf_doc = pdf_doc;
	priv->page = 1;

	if (GTK_WIDGET_REALIZED (pdf_view))
		render_page (pdf_view);
}

static void
pv_realize (GtkWidget *widget)
{
	PdfView *pdf_view;

	g_return_if_fail (IS_NON_NULL_PDF_VIEW (widget));

	GTK_WIDGET_CLASS (parent_class)->realize (widget);

	pdf_view = PDF_VIEW (widget);
	if (pdf_view->priv->pdf_doc)
		render_page (pdf_view);
}

static void
pv_destroy (GtkObject *object)
{
	PdfView *pdf_view = PDF_VIEW (object);
	PdfViewPrivate *priv;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_PDF_VIEW (object));

#ifdef PDF_DEBUG
	g_message ("Destroying PdfView");
#endif

	priv = pdf_view->priv;

	if (priv->out) {
		delete priv->out;
		priv->out = NULL;
	}

	if (priv->gh) {
		gtk_gesture_handler_destroy (priv->gh);
		priv->gh = NULL;
	}
	
	GNOME_CALL_PARENT (GTK_OBJECT_CLASS, destroy, (object));

	priv->pixmap = NULL;
}

static void
pv_finalize (GObject *object)
{
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_PDF_VIEW (object));

	g_free ((PDF_VIEW (object))->priv);

	GNOME_CALL_PARENT (G_OBJECT_CLASS, finalize, (object));
}

static void
class_init (PdfViewClass *klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	parent_class = GTK_EVENT_BOX_CLASS (gtk_type_class (PARENT_TYPE));

	pdf_view_signals [ZOOM_IN_SIGNAL] = gtk_signal_new (
		"zoom_in",
		GTK_RUN_LAST,
		G_TYPE_FROM_CLASS (object_class),
		GTK_SIGNAL_OFFSET (PdfViewClass, zoom_in),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	pdf_view_signals [ZOOM_OUT_SIGNAL] = gtk_signal_new (
		"zoom_out",
		GTK_RUN_LAST,
		G_TYPE_FROM_CLASS (object_class),
		GTK_SIGNAL_OFFSET (PdfViewClass, zoom_out),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	pdf_view_signals [ZOOM_TO_FIT_SIGNAL] = gtk_signal_new (
		"zoom_to_fit",
		GTK_RUN_LAST,
		G_TYPE_FROM_CLASS (object_class),
		GTK_SIGNAL_OFFSET (PdfViewClass, zoom_to_fit),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	pdf_view_signals [ZOOM_TO_FIT_WIDTH_SIGNAL] = gtk_signal_new (
		"zoom_to_fit_width",
		GTK_RUN_LAST,
		G_TYPE_FROM_CLASS (object_class),
		GTK_SIGNAL_OFFSET (PdfViewClass, zoom_to_fit_width),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	pdf_view_signals [ZOOM_TO_DEFAULT_SIGNAL] = gtk_signal_new (
		"zoom_to_default",
		GTK_RUN_LAST,
		G_TYPE_FROM_CLASS (object_class),
		GTK_SIGNAL_OFFSET (PdfViewClass, zoom_to_default),
		gtk_marshal_NONE__NONE,
		GTK_TYPE_NONE, 0);

	object_class->destroy = pv_destroy;
	G_OBJECT_CLASS (klass)->finalize = pv_finalize;

	widget_class->realize = pv_realize;
	widget_class->key_press_event = pv_key_press_event;
	widget_class->button_press_event = pv_button_press_event;
}

static void
init (PdfView *pdf_view)
{
	PdfViewPrivate *priv;

	priv = g_new0 (PdfViewPrivate, 1);
	pdf_view->priv = priv;

	priv->w       = 512;
	priv->h       = 682;
	priv->zoom    = 72.0 * 1.2;
	priv->page    = 1;
}

PdfView *
pdf_view_construct (PdfView *pdf_view)
{
	GtkGestureHandler *gh;

	g_return_val_if_fail (pdf_view != NULL, NULL);
	g_return_val_if_fail (IS_PDF_VIEW (pdf_view), NULL);

	/* Widget */
	GTK_OBJECT_SET_FLAGS (GTK_OBJECT (pdf_view), GTK_CAN_FOCUS);
	
	/* Gesture handler */
	gh = gtk_gesture_handler_new (GTK_WIDGET (pdf_view));
	pdf_view->priv->gh = gh;
	
	gtk_gesture_add_callback (gh, "012",
				  GTK_GESTURE_FUNC (gesture_page_next_event_cb),
				  pdf_view, NULL);
	gtk_gesture_add_callback (gh, "210",
				  GTK_GESTURE_FUNC (gesture_page_prev_event_cb),
				  pdf_view, NULL);
	gtk_gesture_add_callback (gh, "630",
				  GTK_GESTURE_FUNC (gesture_page_first_event_cb),
				  pdf_view, NULL);
	gtk_gesture_add_callback (gh, "036",
				  GTK_GESTURE_FUNC (gesture_page_last_event_cb),
				  pdf_view, NULL);

	gtk_gesture_add_callback (gh, "048",
				  GTK_GESTURE_FUNC (gesture_zoom_in_event_cb),
				  pdf_view, NULL);
	gtk_gesture_add_callback (gh, "840",
				  GTK_GESTURE_FUNC (gesture_zoom_out_event_cb),
				  pdf_view, NULL);

	return pdf_view;
}

PdfView *
pdf_view_new (void)
{
	PdfView *pdf_view;

	pdf_view = PDF_VIEW (gtk_type_new (TYPE_PDF_VIEW));

	return pdf_view_construct (pdf_view);
}


E_MAKE_TYPE (pdf_view, "PdfView", PdfView, class_init, init, PARENT_TYPE)

END_EXTERN_C
