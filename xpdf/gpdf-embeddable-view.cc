/* -*- Mode: C++; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/**
 * application/x-pdf embeddable view
 *
 * Author:
 *   Michael Meeks <michael@ximian.com>
 *   Martin Kretzschmar <Martin.Kretzschmar@inf.tu-dresden.de>
 *
 * Copyright 1999, 2000 Ximian, Inc.
 * Copyright 2002 Martin Kretzschmar
 */

#include "gpdf-embeddable-view.h"
#include "gpdf-util.h"
#include "pdf-view.h"

#define PDF_DEBUG

BEGIN_EXTERN_C

#define PARENT_TYPE BONOBO_VIEW_TYPE
static BonoboViewClass *parent_class = NULL;

static void
page_first_cb (BonoboUIComponent *uih, void *data, const char *path)
{
	pdf_view_page_first (PDF_VIEW (data));
}

static void
page_next_cb (BonoboUIComponent *uih, void *data, const char *path)
{
	pdf_view_page_next (PDF_VIEW (data));
}

static void
page_prev_cb (BonoboUIComponent *uih, void *data, const char *path)
{
	pdf_view_page_prev (PDF_VIEW (data));
}

static void
page_last_cb (BonoboUIComponent *uih, void *data, const char *path)
{
	pdf_view_page_last (PDF_VIEW (data));
}


/*
 * GtkWidget key_press method override
 *
 * Scrolls the window on keypress
 */
static gint
key_press_event_cb (GtkWidget *widget, GdkEventKey *event,
		    GPdfEmbeddableView *view)
{
	float delta;
	GtkAdjustment *adj;

	switch (event->keyval) {
	case GDK_Up:
		adj = gtk_scrolled_window_get_vadjustment
			(GTK_SCROLLED_WINDOW (view->scrolled_window));

		if (event->state & GDK_CONTROL_MASK)
			delta = adj->step_increment * 3;
		else
			delta = adj->step_increment;

		adj->value = CLAMP (adj->value - delta,
				    adj->lower, adj->upper - adj->page_size);

		gtk_adjustment_value_changed (adj);
		break;
	case GDK_Down:
		adj = gtk_scrolled_window_get_vadjustment
			(GTK_SCROLLED_WINDOW (view->scrolled_window));

		if (event->state & GDK_CONTROL_MASK)
			delta = adj->step_increment * 3;
		else
			delta = adj->step_increment;

		adj->value = CLAMP (adj->value + delta,
				    adj->lower, adj->upper - adj->page_size);
		gtk_adjustment_value_changed (adj);
		break;
	case GDK_Left:
		adj = gtk_scrolled_window_get_hadjustment
			(GTK_SCROLLED_WINDOW (view->scrolled_window));

		if (event->state & GDK_CONTROL_MASK)
			delta = adj->step_increment * 3;
		else
			delta = adj->step_increment;

		adj->value = CLAMP (adj->value - delta,
				    adj->lower, adj->upper - adj->page_size);
		gtk_adjustment_value_changed (adj);
		break;
	case GDK_Right:
		adj = gtk_scrolled_window_get_hadjustment
			(GTK_SCROLLED_WINDOW (view->scrolled_window));

		if (event->state & GDK_CONTROL_MASK)
			delta = adj->step_increment * 3;
		else
			delta = adj->step_increment;

		adj->value = CLAMP (adj->value + delta,
				    adj->lower, adj->upper - adj->page_size);
		gtk_adjustment_value_changed (adj);
		break;
	default:
		return FALSE;
	}

	/* Avoid doing keyboard navigation if we handle this event. */
	gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "key_press_event");
	return TRUE;
}


/* Control activation, UI merging */

static BonoboUIVerb verbs [] = {
	BONOBO_UI_VERB ("PageFirst", page_first_cb),
	BONOBO_UI_VERB ("PagePrev", page_prev_cb),
	BONOBO_UI_VERB ("PageNext", page_next_cb),
	BONOBO_UI_VERB ("PageLast", page_last_cb),

	BONOBO_UI_VERB_END
};

static void
gev_set_ui_container (GPdfEmbeddableView *embeddable_view,
		      Bonobo_UIContainer  ui_container)
{
	BonoboUIComponent *ui_component;

	g_return_if_fail (embeddable_view != NULL);
	g_return_if_fail (GPDF_IS_EMBEDDABLE_VIEW (embeddable_view));
	g_return_if_fail (ui_container != CORBA_OBJECT_NIL);

#ifdef PDF_DEBUG
	g_message ("Setting ui container for GPdfEmbeddableView");
#endif

	ui_component = bonobo_control_get_ui_component 
		(BONOBO_CONTROL (embeddable_view));
	g_assert (ui_component != NULL);

	bonobo_ui_component_set_container (ui_component, ui_container);
	bonobo_ui_component_add_verb_list_with_data (ui_component, verbs,
						     /* FIXME + fix verbs */
						     embeddable_view->mainbox);
	/* FIXME change file_name, app_name */
	bonobo_ui_util_set_ui (ui_component, DATADIR,
			       "bonobo-application-x-pdf-ui.xml",
			       "bonobo-application-x-pdf");

#ifdef PDF_DEBUG
	g_message ("Created view menus");
#endif
}

static void
gev_unset_ui_container (GPdfEmbeddableView *embeddable_view)
{
	BonoboUIComponent *ui_component;

	g_return_if_fail (embeddable_view != NULL);
	g_return_if_fail (GPDF_IS_EMBEDDABLE_VIEW (embeddable_view));


#ifdef PDF_DEBUG
	g_message ("Unsetting ui container for GPdfEmbeddableView");
#endif

	ui_component = bonobo_control_get_ui_component 
		(BONOBO_CONTROL (embeddable_view));
	g_assert (ui_component != NULL);
	
	bonobo_ui_component_unset_container (ui_component);
#ifdef PDF_DEBUG
	g_message ("Removed view menus");
#endif
}	

static void
gev_activate (BonoboControl *control, gboolean activate)
{
	GPdfEmbeddableView *embeddable_view = GPDF_EMBEDDABLE_VIEW (control);

	g_return_if_fail (control != NULL);
	g_return_if_fail (GPDF_IS_EMBEDDABLE_VIEW (control));

	bonobo_view_activate_notify (BONOBO_VIEW (embeddable_view), activate);

#ifdef PDF_DEBUG
	g_message ("View change activation to %d", activate);
#endif
	/*
	 * If we were just activated, we merge in our menu entries.
	 * If we were just deactivated, we remove them.
	 */
	if (activate) {
		Bonobo_UIContainer ui_container;
#ifdef PDF_DEBUG
		g_message ("Activating GPdfEmbeddableView");
#endif
		ui_container = bonobo_control_get_remote_ui_container (control);
		if (ui_container != CORBA_OBJECT_NIL) {
			gev_set_ui_container (embeddable_view, ui_container);
			bonobo_object_release_unref (ui_container, NULL);
		}
	} else {
#ifdef PDF_DEBUG
		g_message ("Deactivating GPdfEmbeddableView");
#endif
		gev_unset_ui_container (embeddable_view);	
	}

	if (BONOBO_CONTROL_CLASS (parent_class)->activate)
		BONOBO_CONTROL_CLASS (parent_class)
			->activate (control, activate);
}

static void
gev_set_pdf_cb (GPdfPersistStream *persist_stream, gpointer user_data)
{
	PDFDoc *pdf_doc = NULL;
	PdfView *pdf_view;

	g_return_if_fail (persist_stream != NULL);
	g_return_if_fail (GPDF_IS_PERSIST_STREAM (persist_stream));
	g_return_if_fail (user_data != NULL);
	g_return_if_fail (GPDF_IS_EMBEDDABLE_VIEW (user_data));

	pdf_doc = gpdf_persist_stream_get_pdf_doc (persist_stream);
	pdf_view = PDF_VIEW (GPDF_EMBEDDABLE_VIEW (user_data)->mainbox);
	if (pdf_doc)
		pdf_view_set_pdf_doc (pdf_view, pdf_doc);       
}	



static void
gev_destroy (GtkObject *object)
{
	GPdfEmbeddableView *embeddable_view = GPDF_EMBEDDABLE_VIEW (object);

	g_return_if_fail (object != NULL);
	g_return_if_fail (GPDF_IS_EMBEDDABLE_VIEW (object));

#ifdef PDF_DEBUG
	g_message ("Destroying GPdfEmbeddableView");
#endif

	if (embeddable_view->scrolled_window) {
		gtk_widget_destroy (embeddable_view->scrolled_window);
		embeddable_view->scrolled_window = NULL;
	}
	embeddable_view->mainbox = NULL;

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
class_init (GPdfEmbeddableViewClass *klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);
	BonoboControlClass *control_class = BONOBO_CONTROL_CLASS (klass);

	parent_class = BONOBO_VIEW_CLASS (gtk_type_class (PARENT_TYPE));

	object_class->destroy = gev_destroy;
	control_class->activate = gev_activate;
}

static void
init (GPdfEmbeddableView *view)
{
}

GPdfEmbeddableView *
gpdf_embeddable_view_construct (GPdfEmbeddableView *embeddable_view,
				GPdfEmbeddable *embeddable)
{
	GtkWidget *sw = gtk_scrolled_window_new (NULL, NULL);

	g_return_val_if_fail (embeddable_view != NULL, NULL);
	g_return_val_if_fail (GPDF_IS_EMBEDDABLE_VIEW (embeddable_view), NULL);
	g_return_val_if_fail (embeddable != NULL, NULL);
	g_return_val_if_fail (GPDF_IS_EMBEDDABLE (embeddable), NULL);

	gtk_signal_connect (GTK_OBJECT (embeddable->priv->persist_stream),
			    "set_pdf",
			    GTK_SIGNAL_FUNC (gev_set_pdf_cb),
			    embeddable_view);

	/* Widget */
	embeddable_view->mainbox = GTK_WIDGET (pdf_view_new ());

	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);

	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (sw), 
					       embeddable_view->mainbox);
	embeddable_view->scrolled_window = sw;

	gtk_signal_connect (GTK_OBJECT (embeddable_view->mainbox),
			    "key_press_event",
			    GTK_SIGNAL_FUNC (key_press_event_cb),
			    embeddable_view);
	
	gtk_widget_show_all (embeddable_view->scrolled_window);

	/* Parent constructor */
	if (!bonobo_view_construct (BONOBO_VIEW (embeddable_view),
				    embeddable_view->scrolled_window)) {
		g_warning ("Cannot construct Bonobo View");
		return NULL;
	}

	return embeddable_view;
}

GPdfEmbeddableView *
gpdf_embeddable_view_new (GPdfEmbeddable *embeddable)
{
	GPdfEmbeddableView *embeddable_view;

	g_return_val_if_fail (embeddable != NULL, NULL);
	g_return_val_if_fail (GPDF_IS_EMBEDDABLE (embeddable), NULL);

	embeddable_view =
		GPDF_EMBEDDABLE_VIEW (gtk_type_new (GPDF_TYPE_EMBEDDABLE_VIEW));

#ifdef PDF_DEBUG
	g_message ("Created new bonobo object view %p", embeddable_view);
#endif

	return gpdf_embeddable_view_construct (embeddable_view, embeddable);
}


E_MAKE_TYPE (gpdf_embeddable_view, "GPdfEmbeddableView", GPdfEmbeddableView,
	     class_init, init, PARENT_TYPE)

END_EXTERN_C
