/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/**
 * pdf view widget
 *
 * Author:
 *   Martin Kretzschmar <Martin.Kretzschmar@inf.tu-dresden.de>
 *   Michael Meeks <michael@ximian.com>
 *
 * Copyright 1999, 2000 Ximian, Inc.
 * Copyright 2002 Martin Kretzschmar
 */

#ifndef PDF_VIEW_H
#define PDF_VIEW_H

#include "gpdf-g-switch.h"
#  include <gtk/gtk.h>
#  include <bonobo.h>
#  include "gtkgesture.h"
#include "gpdf-g-switch.h"

#include "PDFDoc.h"
#include "GOutputDev.h"
#include "gpdf-persist-stream.h"

G_BEGIN_DECLS

#define TYPE_PDF_VIEW            (pdf_view_get_type ())
#define PDF_VIEW(obj)            (GTK_CHECK_CAST ((obj), TYPE_PDF_VIEW, PdfView))
#define PDF_VIEW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), TYPE_PDF_VIEW, PdfViewClass))
#define IS_PDF_VIEW(obj)         (GTK_CHECK_TYPE ((obj), TYPE_PDF_VIEW))
#define IS_PDF_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), TYPE_PDF_VIEW))


typedef struct _PdfView        PdfView;
typedef struct _PdfViewClass   PdfViewClass;
typedef struct _PdfViewPrivate PdfViewPrivate;



struct _PdfView {
	GtkEventBox        parent;

	PdfViewPrivate    *priv;
};

struct _PdfViewClass {
	GtkEventBoxClass parent_class;

	/* Signals */
	void (*zoom_in)           (PdfView *pdf_view);
	void (*zoom_out)          (PdfView *pdf_view);
	void (*zoom_to_fit)       (PdfView *pdf_view);
	void (*zoom_to_fit_width) (PdfView *pdf_view);
	void (*zoom_to_default)   (PdfView *pdf_view);
};

GtkType  pdf_view_get_type        (void);
PdfView *pdf_view_new             (void);
PdfView *pdf_view_construct       (PdfView *pdf_view);

void     pdf_view_set_pdf_doc     (PdfView *pdf_view, PDFDoc *pdf_doc);

void     pdf_view_page_prev       (PdfView *pdf_view);
void     pdf_view_page_next       (PdfView *pdf_view);
void     pdf_view_page_first      (PdfView *pdf_view);
void     pdf_view_page_last       (PdfView *pdf_view);

double   pdf_view_get_zoom        (PdfView *pdf_view);
void     pdf_view_set_zoom        (PdfView *pdf_view, double new_zoom);

double   pdf_view_get_page_width  (PdfView *pdf_view);
double   pdf_view_get_page_height (PdfView *pdf_view);

G_END_DECLS

#endif /* PDF_VIEW_H */
