/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/* very stripped down version of ggv-prefs.h from ggv */

#ifndef GGV_PREFS_H
#define GGV_PREFS_H

extern int ggv_unit_index; /* the unit we want to use for coordinates */

#endif /* GGV_PREFS_H */
