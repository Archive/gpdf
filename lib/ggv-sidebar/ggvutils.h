/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/* very stripped down version of ggvutils.h from ggv */

#ifndef GGV_UTILS_H
#define GGV_UTILS_H

extern const float ggv_unit_factors[];

#endif /* GGV_UTILS_H */
