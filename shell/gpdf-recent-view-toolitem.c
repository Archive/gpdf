/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* Toolitem (button with dropdown menu) for recent files
 *
 * Copyright (C) 2002-2004 Authors
 *
 * Authors:
 *   James Willcox <jwillcox@gnome.org> (code from gedit)
 *   Martin Kretzschmar <martink@gnome.org>
 *
 * GPdf is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * GPdf is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <aconf.h>
#include "gpdf-recent-view-toolitem.h"

#include <libgnome/gnome-macros.h>
#include <glib/gi18n.h>
#include <recent-files/egg-recent-model.h>
#include <recent-files/egg-recent-view.h>
#include <recent-files/egg-recent-view-gtk.h>

#define PARENT_TYPE GTK_TYPE_MENU_TOOL_BUTTON
GNOME_CLASS_BOILERPLATE (GPdfRecentViewToolitem, gpdf_recent_view_toolitem,
                         GtkToggleButton, PARENT_TYPE)

struct _GPdfRecentViewToolitemPrivate {
	GtkTooltips *tooltips;
	GtkWidget *menu;
	EggRecentViewGtk *recent_view;
};

enum {
	ITEM_ACTIVATE_SIGNAL,
	LAST_SIGNAL
};

static guint gpdf_recent_view_toolitem_signals [LAST_SIGNAL];

static void
activate_cb (EggRecentViewGtk *view, EggRecentItem *item, gpointer user_data)
{
	g_return_if_fail (GPDF_IS_RECENT_VIEW_TOOLITEM (user_data));
	
	egg_recent_item_ref (item);
	g_signal_emit (
		G_OBJECT (user_data),
		gpdf_recent_view_toolitem_signals [ITEM_ACTIVATE_SIGNAL],
		0, item);
	egg_recent_item_unref (item);
}

void
gpdf_recent_view_toolitem_set_model (GPdfRecentViewToolitem *toolitem,
				     EggRecentModel *model)
{
	EggRecentViewGtk *view;

	view = toolitem->priv->recent_view;
	egg_recent_view_set_model (EGG_RECENT_VIEW (view), model);
}

static void
gpdf_recent_view_toolitem_dispose (GObject *object)
{
	GPdfRecentViewToolitem *toolitem = GPDF_RECENT_VIEW_TOOLITEM (object);

	if (toolitem->priv->tooltips) {
		g_object_unref (toolitem->priv->tooltips);
		toolitem->priv->tooltips = NULL;
	}

	if (toolitem->priv->menu) {
		g_object_unref (toolitem->priv->menu);
		toolitem->priv->menu = NULL;
	}

	if (toolitem->priv->recent_view) {
		g_object_unref (toolitem->priv->recent_view);
		toolitem->priv->recent_view = NULL;
	}

	GNOME_CALL_PARENT (G_OBJECT_CLASS, dispose, (object));
}

static void
gpdf_recent_view_toolitem_finalize (GObject *object)
{
	GPdfRecentViewToolitem *toolitem = GPDF_RECENT_VIEW_TOOLITEM (object);

	if (toolitem->priv) {
		g_free (toolitem->priv);
		toolitem->priv = NULL;
	}

	GNOME_CALL_PARENT (G_OBJECT_CLASS, finalize, (object));
}

static void
gpdf_recent_view_toolitem_instance_init (GPdfRecentViewToolitem *toolitem)
{
	GPdfRecentViewToolitemPrivate *priv;
	AtkObject *atk_obj;

	GTK_WIDGET_UNSET_FLAGS (toolitem, GTK_CAN_FOCUS);  

	toolitem->priv = priv = g_new0 (GPdfRecentViewToolitemPrivate, 1);

	priv->tooltips = gtk_tooltips_new ();
	g_object_ref (priv->tooltips);
	gtk_object_sink (GTK_OBJECT (priv->tooltips));
	gtk_tool_item_set_tooltip (GTK_TOOL_ITEM (toolitem), priv->tooltips,
				   _("Open a file"), NULL);
	gtk_menu_tool_button_set_arrow_tooltip (
		GTK_MENU_TOOL_BUTTON (toolitem), priv->tooltips,
		_("Open a recently used file"), NULL);

	atk_obj = gtk_widget_get_accessible (GTK_WIDGET (toolitem));
	atk_object_set_name (atk_obj, _("Recent"));

	priv->menu = gtk_menu_new ();
	g_object_ref (priv->menu);
	gtk_object_sink (GTK_OBJECT (priv->menu));
	gtk_widget_show (priv->menu);

	gtk_menu_tool_button_set_menu (GTK_MENU_TOOL_BUTTON (toolitem),
				       priv->menu);

	priv->recent_view = egg_recent_view_gtk_new (priv->menu, NULL);
	egg_recent_view_gtk_show_icons (priv->recent_view, TRUE);
	egg_recent_view_gtk_show_numbers (priv->recent_view, FALSE);
	g_signal_connect_object (G_OBJECT (priv->recent_view), "activate",
				 G_CALLBACK (activate_cb), toolitem, 0);
}

static void
gpdf_recent_view_toolitem_class_init (GPdfRecentViewToolitemClass *klass)
{
	G_OBJECT_CLASS (klass)->dispose = gpdf_recent_view_toolitem_dispose;
	G_OBJECT_CLASS (klass)->finalize = gpdf_recent_view_toolitem_finalize;

	gpdf_recent_view_toolitem_signals [ITEM_ACTIVATE_SIGNAL] = g_signal_new (
		"item_activate",
		G_TYPE_FROM_CLASS (klass),
		G_SIGNAL_RUN_LAST,
		G_STRUCT_OFFSET (GPdfRecentViewToolitemClass, item_activate),
		NULL, NULL,
		g_cclosure_marshal_VOID__BOXED,
		G_TYPE_NONE, 1,
		EGG_TYPE_RECENT_ITEM);

	klass->item_activate = NULL;
}
