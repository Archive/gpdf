# xpdf for Latvian.
# Copyright (C) 2002 Glyph & Cog, LLC and authors
# This file is distributed under the same license as the xpdf package.
# Artis Trops <hornet@navigator.lv>, 2002.
#
msgid ""
msgstr ""
"Project-Id-Version: xpdf\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-08-25 19:55+0200\n"
"PO-Revision-Date: 2003-03-15 19:34+0200\n"
"Last-Translator: Artis Trops  <hornet@navigator.lv>\n"
"Language-Team: Latvian <ll10nt@os.lv>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: gpdf.desktop.in.h:1
msgid "PDF file viewer"
msgstr "PDF failu skatītājs"

#: gpdf.desktop.in.h:2
msgid "Tool for viewing PDF files"
msgstr "Rīks PDF failu skatīšanai"

#: shell/eel-vfs-extensions.c:691
msgid " (invalid Unicode)"
msgstr ""

#: shell/gpdf-recent-view-toolitem.c:191
#, fuzzy
msgid "Open a recently used file"
msgstr "Atvērt failu"

#: shell/gpdf-window-ui.xml.h:1
msgid "About this application"
msgstr "Par šo aplikāciju"

#: shell/gpdf-window-ui.xml.h:2
msgid "Close window"
msgstr "Aizvērt logu"

#: shell/gpdf-window-ui.xml.h:3
msgid "Open"
msgstr "Atvērt"

#: shell/gpdf-window-ui.xml.h:4
msgid "Open a file"
msgstr "Atvērt failu"

#: shell/gpdf-window-ui.xml.h:5
msgid "Quit the program"
msgstr "Iziet no programmas"

#: shell/gpdf-window-ui.xml.h:6
msgid "_About"
msgstr "_Par"

#: shell/gpdf-window-ui.xml.h:7
msgid "_Close"
msgstr "_Aizvērt"

#: shell/gpdf-window-ui.xml.h:8 xpdf/gpdf-control-ui.xml.h:29
msgid "_File"
msgstr "_Fails"

#: shell/gpdf-window-ui.xml.h:9
msgid "_Help"
msgstr "_Palīdzība"

#: shell/gpdf-window-ui.xml.h:10
msgid "_Open..."
msgstr "_Atvērt..."

#: shell/gpdf-window-ui.xml.h:11
msgid "_Quit"
msgstr "_Iziet"

#: shell/gpdf.c:141
msgid "Could not initialize Bonobo!\n"
msgstr "Nevarēju inicializēt Bonobo!\n"

#: shell/gpdf.c:264 shell/gpdf.c:267 xpdf/GNOME_PDF.server.in.in.h:4
#: xpdf/gpdf-control.cc:694 xpdf/gpdf-control.cc:728 xpdf/gpdf-control.cc:747
#: xpdf/gpdf-control.cc:1003
msgid "PDF Document"
msgstr "PDF Dokuments"

#: shell/gpdf.c:434
msgid "Load file"
msgstr "Ielādēt failu"

#: shell/gpdf.c:535
msgid "Derek B. Noonburg, Xpdf author."
msgstr "Derek B. Noonburg, Xpdf autors."

#: shell/gpdf.c:536
msgid "Martin Kretzschmar, GNOME port maintainer."
msgstr "Martin Kretzschmar, GNOME porta uzturētājs."

#: shell/gpdf.c:537
msgid "Michael Meeks, GNOME port creator."
msgstr "Michael Meeks, GNOME porta veidotājs."

#. please localize as R&eacute;mi (R\303\251mi)
#: shell/gpdf.c:539
msgid "Remi Cohen-Scali."
msgstr ""

#: shell/gpdf.c:540
msgid "Miguel de Icaza."
msgstr "Miguel de Icaza."

#: shell/gpdf.c:541
msgid "Nat Friedman."
msgstr "Nat Friedman."

#: shell/gpdf.c:542
msgid "Ravi Pratap."
msgstr "Ravi Pratap."

#: shell/gpdf.c:556
msgid "translator-credits"
msgstr "Artis Trops <hornet@navigator.lv>"

#: shell/gpdf.c:559
#, fuzzy
msgid "Glyph & Cog, LLC and authors"
msgstr "Autortiesības © 1996-2002 Glyph & Cog, LLC un autori"

#: shell/gpdf.c:563 shell/gpdf.c:777 xpdf/GNOME_PDF.server.in.in.h:1
msgid "GNOME PDF Viewer"
msgstr "GNOME PDF Skatītājs"

#: shell/gpdf.c:566
msgid "A PDF viewer based on Xpdf"
msgstr "PDF skatītājs, bāzēts uz Xpdf"

#: xpdf/GNOME_PDF.server.in.in.h:2
msgid "GNOME PDF viewer factory"
msgstr "GNOME PDF skatītāja ražotne"

#: xpdf/GNOME_PDF.server.in.in.h:3
msgid "Nautilus PDF Property Page"
msgstr "Nautilus PDF Rekvizītu Lapa"

#: xpdf/GNOME_PDF.server.in.in.h:5
msgid "PDF document viewer factory"
msgstr "PDF dokumenta skata ražotne"

#: xpdf/gpdf-control-ui.xml.h:1
msgid "Back"
msgstr ""

#: xpdf/gpdf-control-ui.xml.h:2
msgid "Best Fit"
msgstr "Labākais Izmērs"

#: xpdf/gpdf-control-ui.xml.h:3
msgid "Best _Fit"
msgstr "Labākais I_zmērs"

#: xpdf/gpdf-control-ui.xml.h:4
msgid "Expand to a larger size"
msgstr "Paplašināt uz lielāku izmēru"

#: xpdf/gpdf-control-ui.xml.h:5
msgid "First"
msgstr "Pirmā"

#: xpdf/gpdf-control-ui.xml.h:6
msgid "Fit Width"
msgstr "Ietilpst Platumā"

#: xpdf/gpdf-control-ui.xml.h:7
msgid "Fit page _width"
msgstr "Ietilpst lapas _platumā"

#: xpdf/gpdf-control-ui.xml.h:8
msgid "Fo_rward"
msgstr ""

#: xpdf/gpdf-control-ui.xml.h:9
msgid "Forward"
msgstr ""

#: xpdf/gpdf-control-ui.xml.h:10
msgid "Go backward in history"
msgstr ""

#: xpdf/gpdf-control-ui.xml.h:11
msgid "Go forward in history"
msgstr ""

#: xpdf/gpdf-control-ui.xml.h:12
msgid "In"
msgstr "Uz iekšu"

#: xpdf/gpdf-control-ui.xml.h:13
msgid "Last"
msgstr "Pēdējā"

#: xpdf/gpdf-control-ui.xml.h:14
msgid "Next"
msgstr "Nākošā"

#: xpdf/gpdf-control-ui.xml.h:15
msgid "Out"
msgstr "Uz āru"

#: xpdf/gpdf-control-ui.xml.h:16
msgid "Previous"
msgstr "Iepriekšējā"

#: xpdf/gpdf-control-ui.xml.h:17
msgid "Proper_ties"
msgstr "_Rekvizīti"

#: xpdf/gpdf-control-ui.xml.h:18
msgid "Shrink to a smaller size"
msgstr "Samazināt uz mazāku izmēru"

#: xpdf/gpdf-control-ui.xml.h:19
msgid "Size to fit the page"
msgstr "Izmērs ietilpst lapā"

#: xpdf/gpdf-control-ui.xml.h:20
msgid "Size to fit the page width"
msgstr "Izmērs ietilpst lapas platumā"

#: xpdf/gpdf-control-ui.xml.h:21
msgid "View properties of the displayed document"
msgstr "Skatīt parādītā dokumenta rekvizītus"

#: xpdf/gpdf-control-ui.xml.h:22
msgid "View the first page"
msgstr "Skatīt pirmo lapu"

#: xpdf/gpdf-control-ui.xml.h:23
msgid "View the last page"
msgstr "Skatīt pēdējo lapu"

#: xpdf/gpdf-control-ui.xml.h:24
msgid "View the next page"
msgstr "Skatīt nākošo lapu"

#: xpdf/gpdf-control-ui.xml.h:25
msgid "View the previous page"
msgstr "Skatīt iepriekšējo lapu"

#: xpdf/gpdf-control-ui.xml.h:26
msgid "Zoom _In"
msgstr "_Tuvināt"

#: xpdf/gpdf-control-ui.xml.h:27
msgid "Zoom _Out"
msgstr "Tā_lināt"

#: xpdf/gpdf-control-ui.xml.h:28
msgid "_Back"
msgstr ""

#: xpdf/gpdf-control-ui.xml.h:30
msgid "_First Page"
msgstr "Pi_rmā Lapa"

#: xpdf/gpdf-control-ui.xml.h:31
msgid "_Go"
msgstr "_Iet"

#: xpdf/gpdf-control-ui.xml.h:32
msgid "_Last Page"
msgstr "_Pēdējā Lapa"

#: xpdf/gpdf-control-ui.xml.h:33
msgid "_Next Page"
msgstr "_Nākošā Lapa"

#: xpdf/gpdf-control-ui.xml.h:34
msgid "_Previous Page"
msgstr "_Iepriekšējā Lapa"

#: xpdf/gpdf-control-ui.xml.h:35
msgid "_View"
msgstr "_Skats"

#: xpdf/gpdf-control.cc:143
msgid "PDF Properties"
msgstr "PDF Rekvizīti"

#: xpdf/gpdf-control.cc:751
#, fuzzy, c-format
msgid "Loading of %s failed."
msgstr "Ielādēt failu"

#: xpdf/gpdf-link-canvas-item.cc:257 xpdf/gpdf-link-canvas-item.cc:258
msgid "Link"
msgstr ""

#: xpdf/gpdf-link-canvas-item.cc:264 xpdf/gpdf-link-canvas-item.cc:265
msgid "UsingHandCursor"
msgstr ""

#: xpdf/gpdf-links-canvas-layer.cc:195 xpdf/gpdf-links-canvas-layer.cc:196
msgid "Links"
msgstr ""

#. translators: page catalog is a part of the PDF file
#. The last period (.) is missing on purpose
#: xpdf/gpdf-persist-file.cc:144
msgid "The PDF file is damaged. Its page catalog could not be read"
msgstr ""

#: xpdf/gpdf-persist-file.cc:147
msgid ""
"The PDF file is damaged or it is not a PDF file. It could not be repaired"
msgstr ""

#: xpdf/gpdf-persist-file.cc:150
msgid ""
"The PDF file is encrypted and requires a password. This program does not "
"support password-protected files"
msgstr ""

#: xpdf/gpdf-properties-dialog.glade.h:1
msgid "Author:"
msgstr "Autors:"

#: xpdf/gpdf-properties-dialog.glade.h:2
msgid "Created:"
msgstr "Izveidots:"

#: xpdf/gpdf-properties-dialog.glade.h:3
msgid "Creator:"
msgstr "Veidotājs:"

#: xpdf/gpdf-properties-dialog.glade.h:4
msgid "Keywords:"
msgstr "Atslēgvārdi:"

#: xpdf/gpdf-properties-dialog.glade.h:5
msgid "Modified:"
msgstr "Modificēts:"

#: xpdf/gpdf-properties-dialog.glade.h:6
msgid "Number of Pages:"
msgstr "Lapu Skaits:"

#: xpdf/gpdf-properties-dialog.glade.h:7
msgid "Optimized:"
msgstr "Optimizēts:"

#: xpdf/gpdf-properties-dialog.glade.h:8
msgid "PDF Version:"
msgstr "PDF Versija:"

#: xpdf/gpdf-properties-dialog.glade.h:9
msgid "Producer:"
msgstr "Ražotājs:"

#: xpdf/gpdf-properties-dialog.glade.h:10
msgid "Security:"
msgstr "Drošība:"

#: xpdf/gpdf-properties-dialog.glade.h:11
msgid "Subject:"
msgstr "Temats:"

#: xpdf/gpdf-properties-dialog.glade.h:12
msgid "Title:"
msgstr "Virsraksts:"

#: xpdf/gpdf-properties-dialog.glade.h:13 xpdf/pdf-info-dict-util.cc:49
#: xpdf/pdf-properties-display.c:225 xpdf/pdf-properties-display.c:233
#: xpdf/pdf-properties-display.c:241 xpdf/pdf-properties-display.c:249
#: xpdf/pdf-properties-display.c:258 xpdf/pdf-properties-display.c:267
#: xpdf/pdf-properties-display.c:275 xpdf/pdf-properties-display.c:284
#: xpdf/pdf-properties-display.c:292 xpdf/pdf-properties-display.c:301
#: xpdf/pdf-properties-display.c:309 xpdf/pdf-properties-display.c:317
msgid "Unknown"
msgstr "Nzināms"

#: xpdf/nautilus-pdf-property-page.cc:146
#, fuzzy
msgid "URI currently displayed"
msgstr "Iestatīt pašlaik parādīto lapu"

#: xpdf/page-control.c:101
#, c-format
msgid " of %d"
msgstr " no %d"

#: xpdf/page-control.c:187
msgid "Current page"
msgstr ""

#: xpdf/page-control.c:188
msgid ""
"This shows the current page number. To jump to another page, enter its "
"number."
msgstr ""

#: xpdf/pdf-info-dict-util.cc:203
msgid "Encrypted"
msgstr ""

#: xpdf/pdf-info-dict-util.cc:203
msgid "None"
msgstr ""

#. Yes/No will be displayed in the pdf properties dialog
#. in a table as "Optimized: No"
#: xpdf/pdf-info-dict-util.cc:219
msgid "Yes"
msgstr ""

#: xpdf/pdf-info-dict-util.cc:219
msgid "No"
msgstr ""

#~ msgid "Could not open %s"
#~ msgstr "Nevarēju atvērt %s"

#~ msgid "dialog1"
#~ msgstr "dialogs1"

#~ msgid "Can't open a directory"
#~ msgstr "Nevaru atvērt direktoriju"

#~ msgid "File Size:"
#~ msgstr "Faila Izmērs:"

#~ msgid "_First"
#~ msgstr "_Pirmā"

#~ msgid "_Last"
#~ msgstr "Pē_dējā"

#~ msgid "_Next"
#~ msgstr "_Nakošā"

#~ msgid "_Page"
#~ msgstr "_Lapa"

#~ msgid "_Prev"
#~ msgstr "_Iepriešējā"
